[NAME]
cde - package everything required to execute a Linux command on another computer

[SEE ALSO]
.BR cde-exec "(1)"
